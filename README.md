# My Dotfiles

Adapted from [Chris Oliver's](https://github.com/excid3/dotfiles) dotfiles. As Chris says, "You can copy these in and everything is ready for the races."

## Installation

1. Clone this repo

    ```
    mkdir -p ~/code && cd ~/code && git clone https://collen@gitlab.com/collen/dotfiles.git
    ```

1. Install Homebrew

    ```
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    ```

1. Install MacVim, rbenv, ruby-build, and more

    ```
    brew install macvim rbenv ruby-build zsh diff-so-fancy the_silver_searcher
    brew install Caskroom/cask/iterm2
    ```

1. Install Oh-My-ZSH and zsh-autosuggestions plugin

    ```
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    ```

    ```
    cd ~/.oh-my-zsh/custom/plugins
    git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
    ```

1. Install [zsh-z](https://github.com/agkozak/zsh-z) plugin

    ```bash
    git clone https://github.com/agkozak/zsh-z $ZSH_CUSTOM/plugins/zsh-z
    ```

1. Install Janus for Vim

    ```
    curl -L https://bit.ly/janus-bootstrap | bash
    ```

1. Symlink configs

    ```
    ln -s ~/code/dotfiles/zsh/themes/excid3.zsh-theme ~/.oh-my-zsh/themes/excid3.zsh-theme
    ln -s ~/code/dotfiles/zsh/.zshrc ~/.zshrc
    ln -s ~/code/dotfiles/vim/gvimrc.after ~/.gvimrc.after
    ln -s ~/code/dotfiles/git/.gitconfig ~/.gitconfig
    ln -s ~/code/dotfiles/.ssh/config ~/.ssh/config
    ln -s ~/code/dotfiles/gemrc ~/.gemrc
    ```

1. Open iTerm and import color scheme from iterm folder

# Other tips

Here are some other useful commands I like to use:

#### Pretty `git lg`

    ```
    git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --"
    ```
